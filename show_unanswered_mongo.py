import sys
import pymongo

def main(args):

    mongodb_uri = 'mongodb://localhost:27017'
    db_name = 'mydb'

    try:
        connection = pymongo.Connection(mongodb_uri)
        database = connection[db_name]
    except:
        print('Error: Base de Datos Desabilitada.')
        connection = None

    if connection is not None:

        resul = database.answers.find({})

        if resul.count() > 0:
            for answer in resul:
                print answer['pregunta']
        else:
            print('La tabla esta vacia!')


if __name__ == '__main__':
    main(sys.argv[1:])
