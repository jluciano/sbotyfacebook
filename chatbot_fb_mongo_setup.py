#! /usr/bin/env python27
#-*- coding: utf-8 -*-
"""
Agente Virtual Inteligente en Facebook
"""
import sys
import xmpp
from primary import chatbotini
import urllib2
from pymongo import Connection

try:
    conn=Connection()
    db=conn['mydb']
    tab_answers=db['answers']
except:
    print('Error: Unable to connect to database.')
    connection = None

def getNombre(jid):
    try:

        f = urllib2.urlopen("https://www.facebook.com/" + jid)
        url = f.geturl().split("/")
        jid=url[len(url) - 1]
        f.close()
        return jid
    except urllib2.URLError:
        return jid



def present_controller(conn, presence):
    """
    Manejador para añadir automáticamente los usuarios a los servidores jabber (Facebook)
    """
    if presence:
        try:
            if presence.getFrom().getStripped() != chatbotini.LOGING:
                print "-" * 100
                print "%s,%s,%s,%s,%s" % (
                    presence.getFrom().getStripped(),
                    presence.getFrom().getResource(),
                    presence.getType(),
                    presence.getStatus(),
                    presence.getShow())
                print getNombre(presence.getFrom().getNode()[1:])
                print "~" * 100

        except UnicodeEncodeError:
            print "-" * 100
            print "%s,%s,%s,No se puede mostrar nick,%s" % (
                presence.getFrom().getStripped(),
                presence.getFrom().getResource(),
                presence.getType(),
                presence.getShow())
            print "~" * 100

        if presence.getType() == "subscribe":
            jid = presence.getFrom().getStripped()
            chatbotini.MY_LIST.Authorize(jid)


def registrochat(message, email):
    """
    Registra el chat.
    """
    filename = chatbotini.LOGDIR + "/fbook/" + email
    logfile = open(filename, "a")
    logfile.write(message)
    logfile.close()


def step_on(conn):
    """
    Mantiene la conexión activa.
    """
    try:
        conn.Process(1)
    except KeyboardInterrupt:
        chatbotini.connection_log(
            "Interrupcion de teclado (Ctrl+C)\n", "fbook")
        disconnect_bot()
    return 1


def loop_start(conn):
    """
    Inicia el bucle.
    """
    while step_on(conn):
        SHOW.setShow("ax")
#        CONN.send(SHOW)


def disconnect_bot():
    """
    Sale y desconecta el bot.
    """
    print "Exiting."
    chatbotini.connection_log("Sesion terminada\n\n\n", "fbook")
    sys.exit(0)


def cache_read_rpta(email):
    """
    Lee el cache
    """
    rpta = 0
    try:
        cache = open(
            "%s%s_1.txt" % (chatbotini.CACHEDIR, str(email)), "r")
        frace = cache.read()

    except IOError:
        pass

    else:
        if frace.count("cuantos años tienes") > 0 or \
                frace.count("y cual es tu edad"):
            rpta = 1

    finally:
        cache.close()

    return rpta


def cache_write_rpta(message, email):
    """
    Escribe la caché
    """
    try:
        cache = open(
            chatbotini.CACHEDIR + str(email) + "_1.txt", "w")
        cache.write(message)

    except IOError:
        pass

    finally:
        cache.close()


def rpta_fbook(conn, mess):
    """
    Responde a los contactos del Facebook.
    """
    logtime = chatbotini.now()
    text = mess.getBody()
    '''
        #so you can convert to lower case

    substitutions = [
        (u"\xe1", u"a"),
        (u"\xe9", u"e"),
        (u"\xed", u"i"),
        (u"\xf3", u"o"),
        (u"\xfa", u"u"),
        ("+", " mas ")]
    for search, replacement in substitutions:
        text = text.replace(search, replacement)

    #text = text.replace(u"\xbf",u"") ##u"\xbf" = ¿
    '''
    user = mess.getFrom()
    user.lang = "en"   # dup
    senderemail = user.getStripped()

    try:
        message = text.lower().replace("\n", " ").encode("utf-8")
    except AttributeError:
        message = ""

    # Registra mensaje de consulta
    registrochat(("%s <<< %s\n" % (logtime, message)), senderemail)

    remsg = chatbotini.action_process(
        message, senderemail, conn=CONN, mess=mess)

    #Almacena las preguntas que no tienen respuestas
    record_questions_unanswered(
        message, remsg, senderemail, logtime[1:11], logtime[12:20])
    remsg = procesa_db(remsg)
    if remsg:
        extramsg = u""
        '''
        if cache_read_rpta(senderemail) == 1:
            # try except really needed?
            try:
                anios = int(text)

            # TODO: find exception type
            except:
                pass

            else:
                if anios < 5:
                    extramsg = u"Tan joven y ya sabes escribir?"
                if anios > 95:
                    extramsg = (u"Vaya eres la persona más longeva que" +
                                u"estoy conociendo!")
        '''

        message = xmpp.Message(
            to=mess.getFrom(), body=extramsg.encode("utf-8") + remsg,
            typ="chat")
        CONN.send(unicode(message).encode("utf-8").replace(r"\n", "\n"))

    # Registra mensaje de respuesta
    # TODO: mueve el codigo necesario fuera del try block
    try:
        message = message.getBody()
        registrochat(
            ("%s >>> %s\n" % (logtime, message.encode("utf-8"))), senderemail)

    except AttributeError:
        pass

    cache_write_rpta(remsg, senderemail)


def procesa_db(remsg):
    """
    Conecta a la Base de Datos en busca el codigo y reemplaza
    """
    if(remsg.find("$") != -1):
        codigo = remsg[-5:]
        if(codigo == "11111"):
            codigo = "http://www.google.com"
        remsg = remsg[:-6] + "" + codigo
    return remsg


def record_questions_unanswered(message, answer2, email, date, time):
    """
    Records Unanswered Questions
    """
    answer = open("unanswered.txt", "r")
    unanswered = answer.readlines()
    answer.close()
    answer_random = [element.split("\n") for element in unanswered]
    for data in answer_random:
        if answer2.find(data[0])>=0:
            data = (email, date, time, unicode(message, "UTF-8"), 0)
            #print data
            if conn is not None:

                registro = {"correo": email,
                            "fecha":date,
                            "hora": time,
                            "pregunta":  unicode(message, "UTF-8"),
                            "ingreso": 0}
                tab_answers.insert(registro)

if __name__ == "__main__":
    print "\n\n* ChatBot Cliente de Facebook *\n"
    CONN = xmpp.Client(chatbotini.SERVER, debug=[])
    SHOW = xmpp.Presence()

    # Show: dnd, away, ax
    SHOW.setShow("ax")
    CONRES = CONN.connect(server=("chat.facebook.com", 5222))

    chatbotini.connection_log("Iniciando sesion\n", "fbook")
    chatbotini.connection_log(
        "Conectando al servidor (chat.facebook.com)\n", "fbook")

    if not CONRES:
        print "No se puede conectar al servidor %s!" % chatbotini.SERVER

        chatbotini.connection_log(
            "No ha sido posible conectar al servidor jabber (%s)\n" % (
                chatbotini.SERVER), "fbook")
        chatbotini.connection_log("Terminando\n\n\n", "fbook")

        sys.exit(1)

    if CONRES != "tls":
        print (
            "Advertencia: no se puede estabilizar conexion segura - TLS fallo")

    AUTHRES = CONN.auth(chatbotini.LOGING.split("@")[0],
                        chatbotini.LOGINPASSWORD,
                        chatbotini.BOTNAME)

    chatbotini.connection_log("Autenticando\n", "fbook")

    if not AUTHRES:
        print ("No se puede autorizar en %s - comprobar " +
               "nombre de usuario / contrasenia.") % chatbotini.SERVER

        chatbotini.connection_log("Login/Password incorrectos\n", "fbook")
        chatbotini.connection_log("Terminando\n\n\n", "fbook")

        sys.exit(1)

    if AUTHRES != "sasl":
        print """Warning: SASL authentication can not you% s.
        Old method of authentication used!""" % chatbotini.SERVER

    CONN.RegisterHandler("message", rpta_fbook)
    CONN.RegisterHandler("presence", present_controller)
    CONN.sendInitPresence()
    CONN.send(SHOW)

    chatbotini.MY_LIST = CONN.getRoster()

    print "Facebook Login OK"
    chatbotini.connection_log("Sesion iniciada\n", "fbook")

    # Starts Application
    loop_start(CONN)
