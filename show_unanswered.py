#! /usr/bin/env python27
# -*- coding: utf-8 -*-
import sqlite3
CONN = sqlite3.connect('show_unanswered.bd')
CONN_CURSOR = CONN.cursor()
CONN_CURSOR.execute('SELECT * FROM preguntas')

if CONN_CURSOR:
    for row in list(CONN_CURSOR):
        print row[3]

CONN.commit()
CONN_CURSOR.close()
CONN.close()
